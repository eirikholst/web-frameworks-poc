package com.tomra.poc

import org.http4k.core.Filter
import org.http4k.core.HttpHandler
import org.http4k.core.Request

fun getTimingFilter() = Filter { next: HttpHandler ->
  { request: Request ->
    val start = System.currentTimeMillis()
    val response = next(request)
    val latency = System.currentTimeMillis() - start
    println("Request to ${request.uri} took ${latency}ms")
    response
  }
}