package com.tomra.poc

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.HttpStatus
import jakarta.annotation.security.PermitAll

@Controller
class MicronautPocController {

    @Get(uri="/hello", produces=["text/plain"])
    fun index(): String {
        return "Example Response"
    }
}