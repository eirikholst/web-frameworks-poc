package com.tomra.poc

import org.http4k.filter.ServerFilters


fun auth() = ServerFilters.ApiKeyAuth {
  return@ApiKeyAuth it.header("user-id") != null
}