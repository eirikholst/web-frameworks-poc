package com.tomra.poc

import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status
import org.http4k.routing.RoutingHttpHandler
import org.http4k.routing.bind
import org.http4k.routing.path
import org.http4k.routing.routes

fun routes(): RoutingHttpHandler = routes(
    "/ping" bind Method.GET to ping(),
    "/greet/{name}" bind Method.GET to greetHandler()
)

private fun ping() = { _: Request -> Response(Status.OK).body("pong!") }


private fun greetHandler() = { req: Request ->
  val name: String? = req.path("name")
  Response(Status.OK).body("hello ${name ?: "anon!"}")
}