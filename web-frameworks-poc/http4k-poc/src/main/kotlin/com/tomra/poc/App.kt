package com.tomra.poc

import org.http4k.core.RequestContexts
import org.http4k.core.then
import org.http4k.filter.ServerFilters
import org.http4k.server.Jetty
import org.http4k.server.asServer

fun main() {
  ServerFilters.InitialiseRequestContext(RequestContexts())
      .then(getTimingFilter())
      .then(auth())
      .then(routes())
      .asServer(Jetty(9000))
      .start()
}
