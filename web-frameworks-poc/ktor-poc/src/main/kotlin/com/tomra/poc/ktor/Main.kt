package com.tomra.poc.ktor

import com.tomra.poc.ktor.plugins.configureHTTP
import com.tomra.poc.ktor.plugins.configureMonitoring
import com.tomra.poc.ktor.plugins.configureRouting
import com.tomra.poc.ktor.plugins.configureSecurity
import com.tomra.poc.ktor.plugins.configureSerialization
import io.ktor.server.application.Application
import io.ktor.server.engine.embeddedServer
import io.ktor.server.jetty.Jetty


fun main() {
  embeddedServer(Jetty, port = 8080, module = Application::module).start(wait = true)
}

fun Application.module() {
  configureSecurity()
  configureHTTP()
  configureMonitoring()
  configureSerialization()
  configureRouting()
}