# web-frameworks-poc

### Comparisons (updated 01.02.2024)
| name      | github stars | last commit | pros                                                                                                                | cons                           | Maintainer           |
|-----------|--------------|-------------|---------------------------------------------------------------------------------------------------------------------|--------------------------------|----------------------|
| sparkjava | 9.6k         | 10.07.2022  | - we know it<br/>- it works<br/>- its cognitively easy                                                              | - single dev<br/>- no traction |                      |
| ktor      | 12k          | 31.01.2024  | - traction by several contributors<br/>- plugin community (metrics, auth, swaggerUi, etc)<br/> - nice documentation | - kotlin only                  | Jetbrains            |
| quarkus   | 12k          | 31.01.2024  | - supports java and kotlin<br/> - built for kubernetes (?)<br/> - spring boot light                                 | - a bit extensive              | RedHat               |
| micronaut | 5k           | 31.01.2024  | - large community and plugins<br/> - agnostic: can be combined with ktor, spring etc<br/> - HAL / Rest Level 3      | - even more extensive          | Micronaut Foundation |
| http4k    | 2.5k         | 30.01.2024  | - functional toolkit<br/> - tiny<br/> - extendable                                                                  | - kotlin only                  | Community            |
| javalin   | 7k           | 28.01.2024  | - lightweight<br/> - no @Annotations<br/> - no magic, just code<br/> - cognitively easy                             |                                |                      |

![img](img.png)
[src: Kotlin Server Side Frameworks](https://resources.jetbrains.com/storage/products/kotlinconf2018/slides/1_Komparing%20Kotlin%20Server-Side%20Frameworks.pdf)