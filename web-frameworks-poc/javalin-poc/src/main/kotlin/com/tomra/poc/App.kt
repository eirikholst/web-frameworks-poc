package com.tomra.poc

import io.javalin.Javalin
import io.javalin.util.JavalinLogger
import java.util.Timer


fun main() {
  Javalin.create { config ->
    config.http.defaultContentType = "application/json"
    config.http.generateEtags = true
    config.requestLogger.http { ctx, ms ->
      JavalinLogger.info("${ctx.statusCode()} ${ctx.method()} ${ctx.path()} ${ms.toInt()} ms")
    }
  }
      .get("/hello") { ctx -> ctx.result("Hello, World!") }
      .post("/hello") { ctx -> ctx.result(ctx.body()) }
      .start(7070)
}
